package org.inovus.util;

public class MyException extends Exception {
    private static final long serialVersionUID = 7139485004793223815L;

    public MyException(Exception exception) {
        super(exception.toString());
    }
}