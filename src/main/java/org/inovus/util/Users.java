package org.inovus.util;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Pattern;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Users {

    private static final Logger LOGGER = LoggerFactory.getLogger(Users.class);

    private static final Pattern PATTERN_USERNAME
            = Pattern.compile("^[a-zA-Z0-9]+$");

    private static final Pattern PATTERN_PASSWORD_1 = Pattern.compile("[a-z]");

    private static final Pattern PATTERN_PASSWORD_2 = Pattern.compile("[A-Z]");

    private static final Pattern PATTERN_PASSWORD_3 = Pattern.compile("[0-9]");

    private static final String ERROR_MSG1 = "Имя пользователя должно быть длинее 4х символов и состоять из цифр и букв английского алфавита";

    private static final String ERROR_MSG2 = "Пароль недостаточно сложен: Должны быть цифры, заглавные и строчные буквы и длинна минимум 8 символов";

    private static final String ERROR_MSG3 = "Пользователь с таким именем уже существует";

    private static final String SQL0 = "create table if not exists users (username text, password text)";

    private static final String SQL1 = "insert into users (username, password) values (?, ?)";

    private static final String SQL2 = "select username from users where username = ? and password = ?";

    private static final String SQL3 = "select username from users where upper(username) = ?";

    private static DataSource source;

    public static String addUser(String username, String password)
            throws MyException {
        String error = testInput(username, password);
        if (error != null) {
            return error;
        }
        if (exists(username)) {
            return ERROR_MSG3;
        }
        try (PreparedStatement ps = connection().prepareStatement(SQL1)) {
            ps.clearParameters();
            ps.setString(1, username);
            ps.setString(2, password);
            ps.executeUpdate();
        } catch (SQLException exception) {
            LOGGER.error("SQL library error", exception);
            throw new MyException(exception);
        }
        return null;
    }

    public static boolean test(String username, String password)
            throws MyException {
        boolean result = false;
        try {
            try (PreparedStatement ps = connection().prepareStatement(SQL2)) {
                ps.clearParameters();
                ps.setString(1, username);
                ps.setString(2, password);
                try (ResultSet rs = ps.executeQuery()) {
                    result = rs.next();
                }
            }
            return result;
        } catch (SQLException exception) {
            LOGGER.error("SQL library error", exception);
            throw new MyException(exception);
        }
    }

    public static boolean exists(String username) throws MyException {
        boolean result = false;
        try {
            try (PreparedStatement ps = connection().prepareStatement(SQL3)) {
                ps.clearParameters();
                ps.setString(1, username.toUpperCase());
                try (ResultSet rs = ps.executeQuery()) {
                    result = rs.next();
                }
            }
            return result;
        } catch (SQLException exception) {
            LOGGER.error("SQL library error", exception);
            throw new MyException(exception);
        }
    }

    private static String testInput(String username, String password) {
        if (username.length() < 4) {
            return ERROR_MSG1;
        }
        if (!PATTERN_USERNAME.matcher(username).matches()) {
            return ERROR_MSG1;
        }
        if (password.length() < 8
                || !PATTERN_PASSWORD_1.matcher(password).find()
                || !PATTERN_PASSWORD_2.matcher(password).find()
                || !PATTERN_PASSWORD_3.matcher(password).find()) {
            return ERROR_MSG2;
        }
        return null;
    }

    public static Connection connection() throws SQLException {
        if (source == null) {
            init();
        }
        return source.getConnection();
    }

    private static void init() throws SQLException {
        File dbfile = new File(
                System.getProperty("java.io.tmpdir"),
                "junior-3.sqlite"
        );
        PoolProperties p = new PoolProperties();
        p.setDriverClassName("org.sqlite.JDBC");
        p.setUrl("jdbc:sqlite:" + dbfile.getAbsolutePath());

        source = new DataSource(p);
        try (Statement st = source.getConnection().createStatement()) {
            st.execute(SQL0);
        }
    }
}
