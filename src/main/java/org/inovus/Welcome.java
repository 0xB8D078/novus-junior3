package org.inovus;

import java.io.IOException;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Welcome extends HttpServlet {
    private static final long serialVersionUID = 976221600761686944L;
        
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        if (req.getSession().getAttribute("username") == null) {
            res.sendRedirect(req.getContextPath() + "/sign-in?message=welcome");
            return;
        }
        req.setAttribute("hour", getHourString());
        req.getRequestDispatcher("welcome.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        if (Boolean.valueOf(req.getParameter("logout"))) {
            req.getSession().removeAttribute("username");
            res.sendRedirect(req.getContextPath() + "/sign-in?message=exit");
            return;
        }
        doGet(req, res);
    }

    private String getHourString() {
        final int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        if(hour < 6 || hour > 22) {
            return "ночь";
        }
        if(hour < 10) {
            return "утро";
        }
        if(hour < 18) {
            return "день";
        }
        return "вечер";
    }
}
