package org.inovus;

import org.inovus.util.Users;
import org.inovus.util.MyException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;

public class SignIn extends HttpServlet {
    private static final long serialVersionUID = 593145290305970986L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        if (req.getSession().getAttribute("username") != null) {
            res.sendRedirect(req.getContextPath() + "/welcome");
            return;
        }
        req.getRequestDispatcher("sign-in.jsp").forward(req, res);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        if (processForm(req, res)) {
            return;
        }
        doGet(req, res);
    }
    
    private boolean processForm(HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException {

        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username == null || password == null) {
            return false;
        }
        try {
            if (Users.test(username, password)) {
                req.getSession().setAttribute("username", username);
                res.addCookie(new Cookie("username", username));
                res.sendRedirect(req.getContextPath() + "/welcome");
            } else {
                res.sendRedirect(req.getContextPath()
                        + "/sign-in?message=notfound");
            }
        } catch (MyException exception) {
            req.setAttribute("exception", exception.getMessage());
            req.getRequestDispatcher("error.jsp").forward(req, res);
        }
        return true;
    }
        
    public static void printUsername(JspWriter out, HttpServletRequest req)
            throws IOException {

        Cookie[] cookies = req.getCookies();
        if (cookies == null) {
            return;
        }
        for (Cookie c : cookies) {
            if ("username".equals(c.getName())) {
                out.print(c.getValue());
                return;
            }
        }
    }
    
    public static void printMessage(JspWriter out, HttpServletRequest req)
            throws IOException {
        
        String msg = req.getParameter("message");
        if (msg == null) {
            return;
        }
        final String format = "<h1 style=\"color:%s\">%s</h1>";
        switch (msg) {
            case "welcome":
                out.print(String.format(format,
                        "green",
                        "Необходимо ввести учётные данные")
                );
                return;
            case "exit":
                out.print(String.format(format,
                        "orange",
                        "Вы вышли из приложения")
                );
                return;
            case "notfound":
                out.print(String.format(format,
                        "red",
                        "Имя пользователя и пароль не подходят")
                );
        }
    }
    
    public static void printContextPath(JspWriter out, HttpServletRequest req)
            throws IOException {

        out.print(req.getContextPath());
    }
}
