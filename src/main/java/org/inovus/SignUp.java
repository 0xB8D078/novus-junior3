package org.inovus;

import org.inovus.util.Users;
import org.inovus.util.MyException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SignUp extends HttpServlet {
    private static final long serialVersionUID = -349071212009913653L;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        if (req.getSession().getAttribute("username") != null) {
            res.sendRedirect(req.getContextPath() + "/welcome");
            return;
        }
        req.getRequestDispatcher("sign-up.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password1");
        String password2 = req.getParameter("password2");
        if (username == null || password == null) {
            doGet(req, res);
            return;
        }
        if (!password.equals(password2)) {
            req.setAttribute("message", "Пароль и повтор пароля не совпадают");
            doGet(req, res);
            return;
        }
        try {
            String message = Users.addUser(username, password);
            if (message != null) {
                req.setAttribute("message", message);
                doGet(req, res);
                return;
            }
            res.addCookie(new Cookie("username", username));
            req.getSession().setAttribute("username", username);
            res.sendRedirect(req.getContextPath() + "/welcome");
        } catch (MyException exception) {
            req.setAttribute("exception", exception.getMessage());
            req.getRequestDispatcher("error.jsp").forward(req, res);
        }
    }
} 