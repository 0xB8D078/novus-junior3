<%@page import="org.inovus.SignIn"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Вход</title>
    </head>
    <body>
        <% SignIn.printMessage(out, request); %>
        <form action="" method="post">
            <table>
                <tr>
                    <td>Имя пользователя:</td>
                    <td><input type="text" name="username" value="<% SignIn.printUsername(out, request); %>"/></td>
                    <td><a href="<% SignIn.printContextPath(out, request);%>/sign-up">Регистрация</a></td>
                </tr>
                <tr>
                    <td>Пароль:</td>
                    <td><input type="password" name="password" /></td>
                    <td><input type="submit" value="Войти" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>