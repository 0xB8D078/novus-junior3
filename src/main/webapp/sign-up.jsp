<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Регистрация</title>
    </head>
    <body>
        <% if (request.getAttribute("message") != null) { %>
        <h1 style="color:red">${message}</h1>
        <% }%>
        <form action="" method="post">
            <table>
                <tr>
                    <td>Имя пользователя:</td>
                    <td><input type="text" name="username"/></td>
                </tr>
                <tr>
                    <td>Пароль:</td>
                    <td><input type="password" name="password1" /></td>
                </tr>
                <tr>
                    <td>Пароль <i>(повторить)</i>:</td>
                    <td><input type="password" name="password2" /></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Регистрация" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>
