<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Добро пожаловать</title>
    </head>
    <body>
        <h1>Доброе ${hour}, <% out.print(session.getAttribute("username"));%></h1>
        <form action="" method="post">
            <input type="hidden" value="true" name="logout" />
            <input type="submit" value="Выход" />
        </form>
    </body>
</html>
